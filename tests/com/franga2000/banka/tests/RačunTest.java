package com.franga2000.banka.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.franga2000.banka.Račun;

class RačunTest {
	
	final String ŠT_RAČUNA = "SI5612349";

	@Test
	void pologTest() {
		final double polog = 678.23489572130;
		Račun test = new Račun(ŠT_RAČUNA);
		
		assertEquals(0, test.getStanje(), "Začetno stanje ni nič");
		
		test.polog(polog);
		
		assertEquals(polog, test.getStanje(), "Polog ne deluje");
	}
	
	@Test
	void dvigTest() {
		final double dvig = 234.63456456345;
		Račun test = new Račun(ŠT_RAČUNA);
		
		test.dvig(dvig);
		
		assertEquals(-dvig, test.getStanje(), "Polog ne deluje");
	}

}
