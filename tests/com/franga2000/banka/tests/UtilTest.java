package com.franga2000.banka.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.franga2000.util.Util;

class UtilTest {

	@Test
	void cutFirstTest() {
		String[] org = new String[] { "first", "second", "third" };
		String[] expected = new String[] { org[1], org[2] };

		String[] test = Util.cutFirst(org);

		assertEquals(expected.length, test.length, "Lengths don't match");

		assertEquals(expected[0], test[0], "First elements don't match");
	}

}
