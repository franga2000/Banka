package com.franga2000.util;

import java.util.Arrays;

/**
 * Subset of my giant class of useful methods
 * CC Zero - feel free to steal anything
 * @author Miha Franagež
 */
public class Util {
	
	/**
	 * Cuts the first element out of an array.
	 * Useful for passing through command parameters
	 * @param array -
	 * @return Array without the first element
	 */
	public static < T > T[] cutFirst(T[] array) {		
		if (array.length == 1) {
			// Java refuses to let me make an empty array of a generic type
			// So, instead of returning an empty array, it returns null
			// Happy debugging!
			return null;
		} else
			return Arrays.copyOfRange(array, 1, array.length);
	}
}
