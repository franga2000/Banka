package com.franga2000.util;

public class Log {

	static final boolean DEBUG = true; // TODO: Cli param, maybe?
	
	public static final String RESET = "\u001B[0m";
	public static final String BLACK = "\u001B[30;40;1m";
	public static final String RED = "\u001B[31;40;1m";
	public static final String GREEN = "\u001B[32;40;1m";
	public static final String YELLOW = "\u001B[33;40;1m";
	public static final String BLUE = "\u001B[34;40;1m";
	public static final String PURPLE = "\u001B[35;40;1m";
	public static final String CYAN = "\u001B[36;40;1m";
	public static final String WHITE = "\u001B[37;40;1m";

	/**
	 * In 2017, Windows STILL can't show colors in the terminal...
	 */
	static String ansi(String color) {
		if (! System.getProperty("os.name").contains("Windows"))
			return color;
		else
			return "";
	}
	
	public static void debug(String msg) {
		if (DEBUG)
			System.out.println("D: " + msg);
	}
	
	public static void println(String msg) {
		System.out.println("  " + msg);
	}
	
	public static void error(String msg) {
		println(ansi(RED));
	}

}
