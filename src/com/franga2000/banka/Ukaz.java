package com.franga2000.banka;

/**
 * Predstavlja en ukaz. Kdo bi si mislu?
 */
public abstract class Ukaz {
	
	public final String ukaz;
	public final String opis;
	
	public Ukaz(String ukaz, String opis) {
		this.ukaz = ukaz;
		this.opis = opis;
	}
	
	public abstract void zaženi(String... args);
	
	@Override
	public String toString() {
		return ukaz + " - " + opis;
	}
}
