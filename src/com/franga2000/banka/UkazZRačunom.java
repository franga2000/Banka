package com.franga2000.banka;

import com.franga2000.util.Log;
import com.franga2000.util.Util;

/**
 * Predstavlja ukaz, ki operira nad računom
 * 
 */
public abstract class UkazZRačunom extends Ukaz {

	public UkazZRačunom(String ukaz, String opis) {
		super(ukaz, opis);
	}

	/**
	 * Prebere številko računa iz prvega parametra in ga posreduje ukazu 
	 */
	public void zaženi(String... args) {
		for (String param : args)
			Log.debug(param);
		Račun račun = Main.getRačun(args[0]);
		
		if (račun == null) {
			Log.error("Neveljaven račun!");
			return;
		}
		
		zaženi(račun, Util.cutFirst(args));
	}
	
	/**
	 * Izvajalna metoda ukaza
	 * @param račun - Račun, nad katerim naj ukaz operira
	 * @param args - Seznam preostalih parametrov
	 */
	public abstract void zaženi(Račun račun, String... args);

}
