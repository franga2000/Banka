package com.franga2000.banka.ukazi;

import com.franga2000.banka.Main;
import com.franga2000.banka.Ukaz;
import com.franga2000.util.Log;

public class Pomoč extends Ukaz {
	
	public Pomoč() {
		super("pomoč", "Izpiše seznam ukazov");
	}

	public final String ukaz = "pomoč";

	@Override
	public void zaženi(String... args) {
		Log.println("Seznam ukazov: ");
		for (Ukaz ukaz : Main.ukazi) {
			Log.println(" - " + ukaz);
		}
	}

}
