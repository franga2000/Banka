package com.franga2000.banka.ukazi;

import com.franga2000.banka.Ukaz;

public class Izhod extends Ukaz {

	public Izhod() {
		super("izhod", "Zapre program");
	}

	@Override
	public void zaženi(String... args) {
		System.exit(0);
	}

}
