package com.franga2000.banka.ukazi;

import com.franga2000.banka.Račun;
import com.franga2000.banka.UkazZRačunom;
import com.franga2000.util.Log;

public class Stanje extends UkazZRačunom {

	public Stanje() {
		super("stanje", "Izpiše stanje na računu (Uporaba: dvig <št_računa> <vsota>)");
	}
	
	@Override
	public void zaženi(Račun račun, String... args) {
		Log.println("Stanje računa " + račun + ": " + račun.getStanje());
	}
	
}
