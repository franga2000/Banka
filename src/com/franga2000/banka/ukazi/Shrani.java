package com.franga2000.banka.ukazi;

import java.io.File;
import java.io.IOException;

import com.franga2000.banka.Main;
import com.franga2000.banka.Ukaz;
import com.franga2000.util.Log;

public class Shrani extends Ukaz {

	public Shrani() {
		super("shrani", "Shrani podatke v datoteko (Uporaba: shrani <ime datoteke>)");
	}

	@Override
	public void zaženi(String... args) {
		if (args.length < 1) {
			Log.error("Manjka ime datoteke!");
			return;
		}
		
		String ime = String.join("", args);
		File file = new File(ime);
		
		try {
			Main.shrani(file);
			
			Log.println("Uspešno shranjeno");
		} catch (IOException e) {
			Log.error("Napaka pri pisanju datoteke:");
			e.printStackTrace();
		}
	}

}
