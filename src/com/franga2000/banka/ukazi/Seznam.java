package com.franga2000.banka.ukazi;

import com.franga2000.banka.Main;
import com.franga2000.banka.Račun;
import com.franga2000.banka.Ukaz;
import com.franga2000.util.Log;

public class Seznam extends Ukaz {

	public Seznam() {
		super("seznam", "Izpiše seznam računov");
	}

	@Override
	public void zaženi(String... args) {
		for (Račun račun : Main.računi) {
			Log.println(" - " + račun);
		}
	}

}
