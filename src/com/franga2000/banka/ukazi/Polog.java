package com.franga2000.banka.ukazi;

import com.franga2000.banka.Račun;
import com.franga2000.banka.UkazZRačunom;

public class Polog extends UkazZRačunom {

	public Polog() {
		super("polog", "Položi denar na račun: (Uporaba: polog <št_računa> <vsota>)");
	}
	
	@Override
	public void zaženi(Račun račun, String... args) {
		double vsota = Double.parseDouble(args[0]);
		račun.polog(vsota);
	}
	
}
