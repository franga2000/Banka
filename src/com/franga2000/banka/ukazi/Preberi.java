package com.franga2000.banka.ukazi;

import java.io.File;
import java.io.IOException;

import com.franga2000.banka.Main;
import com.franga2000.banka.Ukaz;
import com.franga2000.util.Log;

public class Preberi extends Ukaz {

	public Preberi() {
		super("preberi", "Prebere podatke iz datoteke (Uporaba: preberi <ime datoteke>)");
	}

	@Override
	public void zaženi(String... args) {
		if (args.length < 1) {
			Log.error("Manjka ime datoteke!");
			return;
		}
		
		String ime = String.join("", args);
		File file = new File(ime);
		
		try {
			Main.preberi(file);
			
			Log.println("Uspešno prebrano");
		} catch (IOException e) {
			Log.error("Napaka pri branju datoteke:");
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			Log.error("Napaka v formatu datoteke:");
			e.printStackTrace();
		}
	}

}
