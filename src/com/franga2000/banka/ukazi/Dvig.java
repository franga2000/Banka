package com.franga2000.banka.ukazi;

import com.franga2000.banka.Račun;
import com.franga2000.banka.UkazZRačunom;

public class Dvig extends UkazZRačunom {

	public Dvig() {
		super("dvig", "Dvig denarja z računa (Uporaba: dvig <št_računa> <vsota>)");
	}
	
	@Override
	public void zaženi(Račun račun, String... args) {
		double vsota = Double.parseDouble(args[0]);
		račun.dvig(vsota);
	}
	
}
