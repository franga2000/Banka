package com.franga2000.banka;

import java.io.Serializable;

/**
 * Predstavlja en račun. Unexpected, I know!
 */
public class Račun implements Serializable {
	/**
	 * 
	 */
	private String številkaRačuna;
	private String imetnik = "";
	private double stanje;
	
	public Račun(String številkaRačuna) {
		this.številkaRačuna = številkaRačuna;
//		new Račun(stevilkaRacuna, ""); // TODO: To dela?
	}
	
	Račun(String številkaRačuna, String imetnik) {
		this.številkaRačuna = številkaRačuna;
		this.imetnik = imetnik;
	}
	
	/**
	 * Dvigne denar. 
	 * Dvigne, as in, na bankomatu. Ne dvigne stanja - stanje zniža.
	 * @param vsota - Koliko naj dvigne
	 */
	public void dvig(double vsota) {
		this.stanje -= vsota;
	}
	
	
	/**
	 * Položi denar na račun.
	 * Well, v račun. Če bi ga samo gor položu bi ga veter odpihnu.
	 * @param vsota - Koliko naj položi
	 */
	public void polog(double vsota) {
		this.stanje += vsota;
	}
	
	/**
	 * @return Stanje na računu
	 */
	public double getStanje() {
		return this.stanje;
	}
	
	/**
	 * @return Številka računa. Le kdo bi si muslu...
	 */
	public String getStevilkaRacuna() {
		return this.številkaRačuna;
	}
	
	/**
	 * @return Ime in priimek imetnika računa
	 */
	public String getImetnik() {
		return imetnik;
	}

	/**
	 * Preimenuje imetnika računa
	 */
	public void setImetnik(String imetnik) {
		this.imetnik = imetnik;
	}
	
	/**
	 * @return Zapis računa v obliki Ime Piimek<številka_računa>. Just like old mailing lists!
	 */
	@Override
	public String toString() {
		return imetnik + "<" + številkaRačuna + ">";
	}
}
