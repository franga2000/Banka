package com.franga2000.banka;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import com.franga2000.banka.ukazi.Dvig;
import com.franga2000.banka.ukazi.Izhod;
import com.franga2000.banka.ukazi.Polog;
import com.franga2000.banka.ukazi.Pomoč;
import com.franga2000.banka.ukazi.Preberi;
import com.franga2000.banka.ukazi.Seznam;
import com.franga2000.banka.ukazi.Shrani;
import com.franga2000.banka.ukazi.Stanje;
import com.franga2000.util.Util;

public class Main {
	
	public static ArrayList<Račun> računi = new ArrayList<Račun>();
	
	public static ArrayList<Ukaz> ukazi = new ArrayList<Ukaz>();
	
	static {		
		// TODO: Je vse tu?
		ukazi.addAll(Arrays.asList(
			new Dvig(),
			new Izhod(),
			new Polog(),
			new Pomoč(),
			new Preberi(),
			new Seznam(),
			new Shrani(),
			new Stanje()
		));
	}
	
	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);
		
		while (true) {
			System.out.print("Vpišite ukaz: ");
						
			String vrstica = in.nextLine();
			String[] params = vrstica.split(" ");
			
			// Prazne vrstice se ignorirajo
			if (params.length < 1)
				continue;
			
			// Prepoznava ukaza
			Ukaz ukaz = getUkaz(params[0]);
			
			// Opozorilo, če je ukaz neveljaven
			if (ukaz == null) {
				System.out.println("Neveljaven ukaz!");
				System.out.println("Za pomoč, vpišite ukaz \"pomoč\"");
				continue;
			}
			
			try {
				ukaz.zaženi(Util.cutFirst(params));
				
			} catch (ArrayIndexOutOfBoundsException e) {
				System.out.println("Premalo podatkov!");
				System.out.println("Za pomoč, vpišite ukaz \"pomoč\"");
				e.printStackTrace();
			} catch (Exception e) {
				System.out.println("Napaka pri procesiraju ukaza!");
				System.out.println("Za pomoč, vpišite ukaz \"pomoč\"");
				e.printStackTrace();
			}
		}
	}
	
	public static Ukaz getUkaz(String str_ukaz) {
		for (Ukaz ukaz : ukazi) {
			if (ukaz.ukaz.equalsIgnoreCase(str_ukaz)) 
				return ukaz;
		}
		return null;
	}

	public static Račun getRačun(String stevilkaRacuna) {
		for (Račun račun : računi) {
			if (račun.getStevilkaRacuna().equals(stevilkaRacuna)) 
				return račun;
		}
		return null;
	}

	public static void shrani(File file) throws IOException {
		FileOutputStream out = new FileOutputStream(file);
		ObjectOutputStream oos = new ObjectOutputStream(out);
		oos.writeObject(računi);
		oos.flush();
		oos.close();
	}
	
	@SuppressWarnings("unchecked")
	public static void preberi(File file) throws ClassNotFoundException, IOException {
		FileInputStream in = new FileInputStream(file);
		ObjectInputStream ois = new ObjectInputStream(in);
		Main.računi = (ArrayList<Račun>) ois.readObject();
		ois.close();
	}
}
